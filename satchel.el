;;; satchel.el --- Tabs as satchels for buffers -*- lexical-binding: t; -*-
;;
;;
;; Author: Rahguzar <rahguzar@zohomail.eu>
;; Maintainer: Rahguzar <rahguzar@zohomail.eu>
;; Created: May 11, 2023
;; Modified: May 11, 2023
;; Version: 0.0.1
;; Keywords: convenience frames tools
;; Homepage: https://codeberg.org/rahguzar/satchel
;; Package-Requires: ((emacs "27.1") (compat "29.1"))
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;; This package is an attempt to augment tabs with buffer lists which are
;; specific to the tab. The goal is remain as close the design of
;; `tab-bar-mode' as possible.
;;
;;; Code:

(require 'tab-bar)
(require 'bookmark)
(eval-when-compile (require 'cl-lib))

;;; Variables
(defvar satchel-buffer-predicate t
  "A condition to determine which buffers should be added to a tab.
See `buffer-match-p' for different forms, this condition can take. A buffer is
added to the tab only if this condition is satisfied. Similarly when adding a
buffer to tab interactively, only those buffers which satisfy this condition are
offered for completion.")

(defvar satchel-buffer-policy 'switch
  "Determines what happens if current buffer is in a tab other than current tab.
It is used when a window displays a buffer which satisfies
`satchel-buffer-predicate' but is not in current tab. If it is nil do nothing,
if is the symbol switch, switch to a tab containing this buffer and display the
buffer. For any other value, add this buffer to the current tab.")

(defvar satchel-auto-bookmark 'explicitly-named
  "Determines which tabs are automatically bookmarked upon closing.
If it is t all tabs are bookmarked. If it is nil no tabs are bookmarked.
If it is the symbol explicitly-named only those tabs are bookmarked which have
explicit names. For all other values a tab is bookmarked only if a bookmark for
a tab of that name already exists.")

(defvar satchel-kill-buffers-on-tab-close t
  "If non-nil when a tab closes, kill all the buffers exclusive to the tab.")

(defvar satchel--restore nil
  "A list of settings to restore when turning off `satchel-mode'.")

;;; Buffer Management
;;;; Utitlity Definitions
(defmacro satchel--get-tab-buffers (&optional frame)
  "Get the current tab on FRAME."
  `(alist-get 'buffers
    (alist-get 'current-tab
               (frame-parameter (or ,frame (selected-frame)) 'tabs))))

(defun satchel--read-buffer (prompt &optional pred)
  "Read a buffer satisfying PRED with PROMPT."
  (read-buffer
   prompt (buffer-name (current-buffer)) t
   (lambda (cand) (funcall pred (get-buffer (if (stringp cand) cand (car cand)))))))

(defun satchel-buffer-in-tab-p (buf &optional tab)
  "Return non-nil if BUF is in TAB."
  (memq buf (alist-get 'buffers (or tab (alist-get 'current-tab (tab-bar-tabs))))))

(defun satchel-tab-buffers (&optional tab)
  "Return buffers in TAB."
  (if tab
      (cl-delete-if-not #'buffer-live-p (alist-get 'buffers tab))
    (cl-callf2 cl-delete-if-not #'buffer-live-p (satchel--get-tab-buffers))))

(defun satchel--switch (buf tab)
  "Switch to BUF in TAB."
  (switch-to-prev-buffer)
  (tab-bar-switch-to-tab (alist-get 'name tab))
  (unless (get-buffer-window buf)
    (display-buffer buf '(display-buffer-use-some-window))))

(defun satchel--maybe-add-buffer (&optional frame)
  "Possibly add current buffer to current tab on FRAME.
Whether it is added depends on `satchel-buffer-predicate' and
`satchel-buffer-policy'."
  (if-let ((buf (current-buffer))
           (addp (and (not (satchel-buffer-in-tab-p buf))
                      (buffer-match-p satchel-buffer-predicate buf)))
           (tabs (cl-remove buf (tab-bar-tabs)
                            :test-not #'memq :key #'satchel-tab-buffers))
           ((eq 'switch satchel-buffer-policy)))
      (run-with-timer 0 nil #'satchel--switch buf (car tabs))
    (and addp satchel-buffer-policy (satchel-add-buffer buf frame))))

(defun satchel--buffer-predicate (buf)
  "Function to use as the buffer-predicate of a frame.
Returns non-nil if BUF is in the tab or else there is no buffer in tab
and BUF is the fallback buffer."
  (let ((bufs (or (satchel-tab-buffers) `(,(satchel-fallback-buffer)))))
    (memq buf bufs)))

(defun satchel--switch-buffer-skip (_ buf _)
  "Return non-nil if BUF is not in the current tab."
  (not (satchel--buffer-predicate buf)))

(defun satchel--resurrect-fallback-buffer ()
  "Recreate fallback buffer when it is deleted."
  (run-with-timer 0 nil #'satchel-fallback-buffer))

(defun satchel-dont-kill-if-in-other-tabs ()
  "Function to prevent killing a buffer if it is present in multiple tabs.
It can be added to `kill-buffer-query-functions'. If a buffer is present in
tabs other than the current one, this function removes it from the current
tab and returns nil thus preventing the buffer from getting killed."
  (let ((current (alist-get 'current-tab (tab-bar-tabs))))
    (catch 'found
      (dolist (frame (frame-list))
        (and (not (frame-parent frame))
             (when (cl-find-if (lambda (tab)
                                 (and (satchel-buffer-in-tab-p (current-buffer) tab)
                                      (not (eq (cdr tab) current))))
                               (tab-bar-tabs frame))
               (satchel-remove-buffer)
               (throw 'found nil))))
      t)))

(defun satchel-fallback-buffer ()
  "Fallback buffer used when creating a new tab.
It is computed using `initial-buffer-choice'."
  ;; The implementation is from `startup.el'. Upstream this one day.
  (with-current-buffer
      (cond ((stringp initial-buffer-choice)
             (find-file-noselect initial-buffer-choice))
            ((functionp initial-buffer-choice)
             (funcall initial-buffer-choice))
            ((eq initial-buffer-choice t)
             (get-scratch-buffer-create))
            (t
             (error "`initial-buffer-choice' must be a string, a function, or t")))
    (add-hook 'kill-buffer-hook #'satchel--resurrect-fallback-buffer nil t)
    (current-buffer)))

(defun satchel-after-make-frame (frame &optional no-init)
  "Function added to `after-make-frame-functions'.
NO-INIT means FRAME has already been initialized"
  (unless (eq (frame-parameter frame 'minibuffer) 'only)
    (set-frame-parameter frame 'satchel--restore
                         (frame-parameter frame 'buffer-predicate))
    (set-frame-parameter frame 'buffer-predicate #'satchel--buffer-predicate)
    (unless (alist-get 'name (alist-get 'current-tab (tab-bar-tabs)))
      (tab-bar-rename-tab ""))
    (unless no-init
      (with-selected-frame frame (switch-to-buffer (satchel-fallback-buffer))))))

;;;; Interactive Commands
(defun satchel-add-buffer (&optional buf-or-name frame switchp)
  "Add a buffer to current tab with completion and switch to it.
FRAME should be frame to use and defaults to selected frame. BUF-OR-NAME
defaults to current buffer if called from Lisp. SELECTP if non-nil means switch
to the added buffer."
  (interactive (list (satchel--read-buffer
                      "Add buffer: "
                      (lambda (buf)
                        (and (not (satchel-buffer-in-tab-p buf))
                             (buffer-match-p satchel-buffer-predicate buf))))
                     nil t))
  (cl-callf2 cl-delete-if-not #'buffer-live-p (satchel--get-tab-buffers frame))
  (cl-pushnew (get-buffer (or buf-or-name (current-buffer)))
              (satchel--get-tab-buffers frame))
  (when switchp (switch-to-buffer buf-or-name)))

(defun satchel-remove-buffer (&optional buf-or-name frame)
  "Remove the current buffer from the current tab.
If called with prefix arg offer buffers in tabs for completion and remove the
chosen buffer. FRAME should be frame to use and defaults to selected frame.
BUF-OR-NAME defaults to current buffer if called from Lisp."
  (interactive (list (if current-prefix-arg
                         (satchel--read-buffer
                          "Remove buffer: " #'satchel-buffer-in-tab-p)
                       (current-buffer))))
  (setq buf-or-name (get-buffer (or buf-or-name (current-buffer))))
  (cl-callf2 cl-delete-if-not #'buffer-live-p (satchel--get-tab-buffers frame))
  (when (eq (window-buffer) buf-or-name)
    (if (length> (satchel-tab-buffers) 1)
        (switch-to-prev-buffer)
      (switch-to-buffer (satchel-fallback-buffer))))
  (cl-callf2 delq buf-or-name (satchel--get-tab-buffers frame)))

(defun satchel-switch-to-buffer (&optional buf-or-name)
  "Switch to a buffer for the current tab.
BUF-OR-NAME defaults to current buffer if called from Lisp."
  (interactive (list (satchel--read-buffer
                      "Switch to buffer: " #'satchel-buffer-in-tab-p)))
  (switch-to-buffer buf-or-name))

;;; Bookmarking
;;;; Interactive Bookmarking
(defun satchel-bookmark (&optional tab name no-overwrite)
  "Make a bookmark record for current tab and store it.
When called from Lisp, TAB is the tab to bookmark. The return value is the
bookmark record. If NAME is non-nil bookmark is stored with name NAME. If
NAME is symbol t, the name of tab is used. For interactive use, user is also
prompted if called with prefix arg and corresponds to symbol `prompt' as
NAME. NO-OVERWRITE has same meaning as in `bookmark-store' except if it is
the symbol `prompt' in which case user is prompted for a name if a bookmark
with that name already exists."
  (interactive (list nil (if current-prefix-arg 'prompt t) 'prompt))
  (setq tab (or tab (alist-get 'current-tab (tab-bar-tabs))))
  (let-alist tab
    (when (or (eq name 'prompt)
              (and (member .name (bookmark-all-names))
                   (eq no-overwrite 'prompt)))
      (setq name (read-string
                   (format "%sChoose bookmark name: "
                           (if (member .name (bookmark-all-names))
                               (format "Bookmark %s exists: " .name)
                             ""))
                   nil nil .name)))
    (let* ((bmark (lambda (buf) (with-current-buffer buf
                                  (with-demoted-errors "Error: %S"
                                    (bookmark-make-record)))))
           (bufs (mapcar bmark (cl-delete-if-not #'buffer-live-p .buffers)))
           (rec (list (list 'tab
                            (cons 'name .name)
                            (cons 'explicit-name .explicit-name)
                            (cons 'ws (or .ws (window-state-get nil t)))
                            (cons 'buffers (cl-remove nil bufs)))
                      (cons 'handler #'satchel-bookmark-handler))))
      (when name
        (bookmark-store (if (stringp name) name .name) rec no-overwrite))
      (when (memq nil bufs)
        (message "Warning: some of the buffers in tab couldn't be bookmarked."))
      rec)))

(defun satchel-bookmark-jump (&optional tabmk)
  "Restore a bookmarked tab TABMK."
  (interactive)
  (bookmark-maybe-load-default-file)
  (let ((bookmark-alist (cl-remove 'satchel-bookmark-handler bookmark-alist
                                   :test-not #'eq
                                   :key #'bookmark-get-handler))
        (bookmark-fringe-mark nil))
    (unless tabmk (setq tabmk (bookmark-completing-read "Restore tab")))
    (bookmark-jump tabmk #'ignore)))

;;;; Handler
(defun satchel-bookmark-handler (bmk)
  "Bookmark handler for a tab bookmark record BMK."
  (let-alist (cdr (assq 'tab bmk))
    (tab-bar-switch-to-tab .name)
    (dolist (buf-bmk .buffers)
      (with-demoted-errors "Error: %S"
        (bookmark-jump buf-bmk #'satchel-add-buffer)))
    (window-state-put .ws (frame-root-window))
    (when .ws.buffer (set-buffer (car .ws.buffer)))))

;;;; Auto Bookmarking
(defun satchel--maybe-bookmark (&optional tab name)
  "Bookmark TAB depending on `satchel-auto-bookmark'.
If NAME is non-nil look for a bookmakrk with NAME instead of using tab name."
  (setq tab (or tab (alist-get 'current-tab (tab-bar-tabs))))
  (setq name (or (and (stringp name) name) (alist-get 'name tab)))
  (when (or (eq satchel-auto-bookmark t)
            (and (eq satchel-auto-bookmark 'explicitly-named)
                 (alist-get 'explicit-name tab))
            (and satchel-auto-bookmark
                 (eq 'satchel-bookmark-handler
                     (bookmark-get-handler (bookmark-get-bookmark name t)))))
    (satchel-bookmark tab name)))

(defun satchel--bookmark-on-delete (&optional frame)
  "Bookmark tabs on FRAME depending on `satchel-auto-bookmark'."
  (if frame (mapc #'satchel--maybe-bookmark (tab-bar-tabs frame))
    (dolist (frame (frame-list))
      (with-selected-frame frame
        (mapc #'satchel--maybe-bookmark (tab-bar-tabs frame))))))

;;; Misc
(defun satchel--maybe-kill-buffers (&optional tab)
  "Kill buffers in TAB if `satchel-kill-buffers-on-tab-close' is non-nil."
  (when satchel-kill-buffers-on-tab-close
    (setq tab (or tab (alist-get 'current-tab (tab-bar-tabs))))
    (let ((other-bufs nil))
      (dolist (frame (frame-list))
        (dolist (other-tab (tab-bar-tabs frame))
          (unless (eq tab (cdr other-tab))
            (cl-callf cl-union other-bufs (alist-get 'buffers other-tab)))))
      (dolist (buf (alist-get 'buffers tab))
        (unless (memq buf other-bufs)
          (kill-buffer buf))))))

(defun satchel--on-tab-close (&optional tab _)
  "Possibly bookmark the TAB and kill buffers in it before it closes."
  (satchel--maybe-bookmark tab)
  (satchel--maybe-kill-buffers))

;;; Ibuffer
(require 'ibuf-ext)
(define-ibuffer-filter tab "Filter buffers in a given tab QUALIFIER."
  (:reader (let* ((tabs (tab-bar-tabs))
                  (current (alist-get 'name (alist-get 'current-tab tabs)))
                  (tabname (lambda (tab) (alist-get 'name tab))))
             (cl-find
              (completing-read (format-prompt "Choose a tab by name" current)
                               (mapcar tabname tabs) nil t nil nil current)
              tabs :key tabname :test #'equal))
   :description "Filter matching buffers in a tab.")
  (memq (get-buffer buf) (alist-get 'buffers qualifier)))

(defun satchel-ibuffer (&optional other-window-p name noselect shrink formats)
  "A version of ibuffer grouping buffers by tabs.
OTHER-WINDOW-P, NAME, NOSELECT, SHRINK and FORMATS are as in `ibuffer'."
  (interactive "P")
  (ibuffer other-window-p name nil noselect shrink
           (mapcar (lambda (tb) `(,(alist-get 'name tb) (tab . ,tb))) (tab-bar-tabs))
           formats))

;;; Minor mode
;;;###autoload
(define-minor-mode satchel-mode
  "A minor mode to augment tabs with buffer lists."
  :global t
  :lighter "Satchel Mode"
  :group 'tab-bar
  (cl-flet
      ((usevar (sym val)
         (if satchel-mode
             (progn (setf (alist-get sym satchel--restore) (symbol-value sym))
                    (set sym val))
           (set sym (alist-get satchel--restore sym))))
       (usehook (sym fun)
         (if satchel-mode (add-hook sym fun) (remove-hook sym fun))))
    (usevar 'tab-bar-new-tab-choice #'satchel-fallback-buffer)
    (usevar 'tab-bar-tabs-function #'tab-bar-tabs)
    (usevar 'switch-to-prev-buffer-skip #'satchel--switch-buffer-skip)
    (usehook 'window-buffer-change-functions #'satchel--maybe-add-buffer)
    (usehook 'after-make-frame-functions #'satchel-after-make-frame)
    (usehook 'tab-bar-tab-pre-close-functions #'satchel--on-tab-close)
    (usehook 'delete-frame-functions #'satchel--bookmark-on-delete)
    (usehook 'kill-emacs-hook #'satchel--bookmark-on-delete)
    (dolist (frame (frame-list))
      (if satchel-mode
          (satchel-after-make-frame frame t)
        (set-frame-parameter frame 'buffer-predicate
                             (frame-parameter frame 'satchel--restore))
        (set-frame-parameter frame 'satchel--restore nil)))
    (when satchel-mode (satchel-fallback-buffer))))


(provide 'satchel)
;;; satchel.el ends here
